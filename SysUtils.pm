package SysUtils;

#----------------------------------------------------------------------
#
# SysUtils.pm -- Torsten Crass, 2009-2012
#
# Torsten's Perl library of frequently required functionalities
#
# Use your favourite pod processor to extract the documentation,
# e.g. pod2html SysUtils.pm > SysUtils.html
#
# LICENSE
# -------
#
# This library is distributed under the terms of the GNU GPL, version
# 2 (see http://www.gnu.org/licenses/old-licenses/gpl-2.0.html).
# Please note that SysUtils.pm is *not* licenced under the Lesser GLP
# (LGPL), requiring every piece of software making use of this library
# to also be published under the GPL.
#
#----------------------------------------------------------------------


use Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(glob);

use strict;
use warnings;
use Fcntl qw(:DEFAULT :flock);

use POSIX ':sys_wait_h';

our $VERSION = '1.5.1';


=pod

=head1 SysUtils

Provides subroutines for a plethora of recurring basic tasks.

=cut


#--- Global Variables -------------------------------------


=pod

=head2 Global Variables

=over 1

=item * C<$VERSION>

This librarie's version number.

=item * C<$IS_WINDOWS>

C<TRUE> or C<FALSE>, depending on whether or not C<$^O> starts with C<mswin>. As an additional goody, C<File::DosGlob> gets automatically included if Windows is detected.

=item * C<$DIRSEP>

System specific directory separator. 'C<\>' on Windows, 'C</>' else.

=item * C<$DIRSEP_REGEX>

Escaped directory separator -- useful for regular expressions.

=item * C<$PATHSEP>

System specific path separator. 'C<;>' on Windows, 'C<:>' else.

=item * C<$CURRDIR>

The present working directory.

=item * C<$HOMEDIR>

The current user's home directory.

=back

=cut

our $DEFAULT_INI_SECTION = '__DEFAULT__';

our $DIRSEP;
our $DIRSEP_REGEX;
our $PATHSEP;
our $CURRDIR;
our $HOMEDIR;
our $IS_WINDOWS = ($^O =~ m/^mswin/i);
if ($IS_WINDOWS) {
  use File::DosGlob qw(glob);
  $DIRSEP = '\\';
  $DIRSEP_REGEX = '\\\\';
  $PATHSEP = ";";
  $CURRDIR = qx(cmd /c echo %CD%);
  $CURRDIR = &cleanDir(substr($CURRDIR, 0, length($CURRDIR)-1));
  my $env = qx(cmd /c set);
  ($env) = &parseConfigString($env);
  $HOMEDIR = $env->{HOMEDRIVE} . &cleanDir($env->{HOMEPATH});
  $HOMEDIR =~ s/\//\\/g;
}
else {
  use File::Glob qw(glob);
  $DIRSEP = '/';
  $DIRSEP_REGEX = '\/';
  $PATHSEP = ":";
  $CURRDIR = &cleanDir($ENV{PWD});
  $HOMEDIR = &cleanDir($ENV{HOME});
}

our $VISIT_DIRS_BEFORE_FILES = 0;
our $PROCESS_DIRS_BEFORE_FILES = $VISIT_DIRS_BEFORE_FILES;
our $VISIT_FILES_BEFORE_DIRS = 1;
our $PROCESS_FILES_BEFORE_DIRS = $VISIT_FILES_BEFORE_DIRS;
our $PROCESS_DIRS_BEFORE_VISITING = 0;
our $VISIT_DIRS_BEFORE_PROCESSING = 2;
our $PROCESS_DOT_FILES = 4;
our $PROCESS_DOT_DIRS = 8;
our $PROCESS_DOT_ENTREIS = 12;

#--- Private globals --------------------------------------

my $MARKER_COUNTER = 0;

my %CHILDREN = ();

#--- Processing Command-Line Options ----------------------


=pod

=head2 Processing Command-Line Options

=over 1

=item * C<(hashref $opts, arrayref $args, string $err) parseOptions((hashref|arrayref) $optDescr [int $maxArgs [, @argv]])>

Parses C<@argv> (or C<@ARGV>, if not given) according to the option description given in C<$optDescr> and optionally checks whether the number of non-optional arguments doesn't exceed a specified maximum number (C<$maxArgs>; default is -1, which means no upper limit).

Returns a reference to a hash containing pairs of option names and corresponding values, a reference to an array containing the non-optional arguments and an error description, if a parsing error has occured (C<undef> else).

The possible options' descriptions are passed as a reference to either an array or a hash. In the latter case, the hash values may be references to (scalar or array, see below) variables which may contain default values for their corresponding options and which will finally contain the option values given at the command line (in addition to the C<$opts> hashref returned by this subroutine).

In both cases, each option gets described by a string of the following format:

C<long-name[|short-name][(=|@|+)[default]])>

Each option is identified by a long name, which is used for specifying the option at the command line in the C<--long-name> format. C<short-name> is a one-character abbreviation (like C<x>) which can alternatively be used for specifying the option: C<-x>. If an option description contains an C<=> character at the position specified above, it is considered an optional argument, i.e. the next string specified at the command line (like  C<--long-name foobar>, C<--long-name=foobar> or C<-x foobar>) is considered this option's value (which will be stored in C<< $opt->{long-name} >>). If the option descriptions have been passed as a hashref and the option's hash value is a reference to a scalar variable, the option value will also be stored therein. If the C<=> is followed by further characters, the string formed by them is taken as the option's default value.

In case an option description ends with either an C<@> or a C<+> character, it is considered an optional argument which may be specified multiple times at the command line. All values specified with the different occurances are returned as a reference to a list. If the option description has been given in the form of a hashref, the option description's value may be a reference to a list variable; after parsing, this variable will hold all option values, in addition to the list referenced in by C<< $opts->{long-name} >>. Defaults may be specified either as a string following the C<@> or C<+> character (being converted to a one-element list), or by the passed listref in case of a hashref-based option description. The difference between the C<@> and C<+> forms is that in case of C<@> any given option values will replace the defaults, whereas in the C<+> case all given options will be appended to the default list.

Option descriptions without C<=>, C<@> or C<+> define boolean options which do not expect their value to be specified as separate string on the command line; rather, their values are set to C<TRUE> if the option is given and to C<FALSE> if not. As a special feature, boolean options' short names can be globbed, i.e. instead of 'C<-a -b -c>', one could also write 'C<-abc>'.

=cut
sub parseOptions {
  my $optDescr = shift;

  my %optDescr = ();
  if (ref($optDescr) eq 'ARRAY') {
    foreach my $opt (@{$optDescr}) {
      $optDescr{$opt} = undef;
    }
  }
  elsif (ref($optDescr) eq 'HASH') {
    %optDescr = %{$optDescr};
  }

  my $maxFileNames = -1;
  if (@_) {
    $maxFileNames = shift;
  }

  my @args = @ARGV;
  if (@_) {
    @args = @_;
  }

  my %optVars = ();
  my %longNames = ();
  my %clearList = ();
  my %short2long = ();
  my $shortOptsBool = "";
  my $shortOptsVal = "";
  my %values = ();
  my $err = undef;

  foreach my $opt (keys(%optDescr)) {
    if ($opt =~ m/^(\w+[\-\w]*)/) {
      my $longName = $1;
      $longNames{$longName} = $1;
      if (ref($optDescr{$opt}) ne '') {
        $optVars{$longName} = $optDescr{$opt};
      }
      my $shortName = "";
      if ($opt =~ m/^\w+[\-\w]*\|([\w\?])/) {
        $shortName = $1;
        $short2long{$shortName} = $longName;
      }
      if ($opt =~ m/^\w+[\-\w]*(\|[\w\?])?=(.*)$/) {
        $values{$longName} = $2;
        if (($values{$longName} eq "") and
            exists($optVars{$longName}) and
            defined($optVars{$longName})) {
          $values{$longName} = ${($optVars{$longName})};
        }
        $shortOptsVal .= $shortName;
      }
      elsif ($opt =~ m/^\w+[\-\w]*(\|[\w\?])?([\@+])(.*)$/) {
        $clearList{$longName} = ($2 eq '@' ? 1 : 0);
        if ($3) {
          $values{$longName} = [$3];
        }
        else {
          $values{$longName} = [];
        }
        if ((scalar(@{$values{$longName}}) == 0) and
            exists($optVars{$longName}) and
            defined($optVars{$longName}) and
            (ref($optVars{$longName}) eq 'ARRAY')) {
          $values{$longName} = $optVars{$longName};
        }
        $shortOptsVal .= $shortName;
      }
      else {
        $values{$longName} = undef;
        $shortOptsBool .= $shortName;
      }
    }
    else {
      die "Option $opt: Long name mandatory!\n"
    }
  }

  my $done = 0;
  my $i = 0;
  while ((not $done) && ($i < scalar(@args))) {
    my $opt = $args[$i];
    $i++;
    if ($opt =~ m/^--(\w+[-\w]+?)(=(.*))?$/) {
      my $name = $1;
      my $val = $3;
      if (exists($longNames{$name})) {
        if (defined($values{$name})) {
          if ($val) {
            splice(@args, $i, 0, $val);
          }
          if ($i < scalar(@args)) {
            if (ref($values{$name}) eq 'ARRAY') {
              if ($clearList{$name}) {
                $values{$name} = [];
                $clearList{$name} = 0;
              }
              push(@{$values{$name}}, $args[$i]);
            }
            else {
              $values{$name} = $args[$i];
            }
            $i++;
          }
          else {
            $err = "Missing value for option $opt!\n";
            $done = 1;
          }
        }
        else {
          if ($val && ($val ne "")) {
            unless ($val =~ m/^(false|no|0)$/i) {
              $values{$name} = 1;
            }
          }
          else {
            $values{$name} = 1;
          }
        }
      }
      else {
        $done = 1;
        $i--;
      }
    }
    elsif ($opt =~ m/^-([\w\?]+)$/) {
      my $names = $1;
      my $len = length($names);
      my %tmpValues = ();
      for (my $pos = 0; $pos < $len; $pos++) {
        my $name = substr($names, $pos, 1);
        my $regex = ($name eq '?' ? '\?' : $name);
        if (exists($short2long{$name})) {
          my $longName = $short2long{$name};
          if ($shortOptsBool =~ m/$regex/) {
            $tmpValues{$longName} = 1;
          }
          elsif ($shortOptsVal =~ m/$regex/) {
            if ($len == 1) {
              if ($i < scalar(@args)) {
                if (ref($values{$longName}) eq 'ARRAY') {
                  if (exists($tmpValues{$longName})) {
                    push(@{$tmpValues{$longName}}, $args[$i]);
                  }
                  else {
                    $tmpValues{$longName} = [$args[$i]];
                  }
                }
                else {
                  $tmpValues{$longName} = $args[$i];
                }
                $i++;
              }
              else {
                $err = "Missing value for option -$name!\n";
                $done = 1;
              }
            }
            else {
              %tmpValues = ();
              $done = 1;
            }
          }
        }
        else {
          %tmpValues = ();
          $done = 1;
          $i--;
        }
      }
      if (%tmpValues) {
        foreach my $name (keys(%tmpValues)) {
          if (ref($tmpValues{$name}) eq 'ARRAY') {
            if ($clearList{$name}) {
              $values{$name} = [];
              $clearList{$name} = 0;
            }
            push(@{$values{$name}}, @{$tmpValues{$name}});
          }
          else {
            $values{$name} = $tmpValues{$name};
          }
        }
      }
    }
    else {
      $done = 1;
      $i--;
    }
  }
  my @files = ();
  for (my $j = $i; $j < scalar(@args); $j++) {
    push (@files, $args[$j]);
  }
  if (($maxFileNames >= 0) and (scalar(@files) > $maxFileNames)) {
    $err = "Too many arguments: " . join(" ", splice(@files, $maxFileNames)) . "\n";
  }
  foreach my $name (keys(%values)) {
    if (exists($optVars{$name})) {
      if (ref($optVars{$name}) eq 'ARRAY') {
        @{($optVars{$name})} = @{$values{$name}};
      }
      else {
        ${($optVars{$name})} = $values{$name};
      }
    }
  }
  return (\%values, \@files, $err, );
}

=pod

=back

=cut

#--- Reading Configuration Files --------------------------


=pod

=head2 Reading Configuration Files

=over 1

=item * C<(hashref $defaultConf, string $err, hashref $allConfSections) parseConfigString(string $confStr)>

Parses the content of the given (multi-line) string and returns in C<$allConfSections> a reference to a hash containing hashrefs holding for each ("Windows-style") ini section the values for the configuration options found therein. All configuration options found before the the first section header is encountered are returned in the C<$defaultConf> hashref (and in the C<$allConfSection> element with C<$DEFAULT_INI_SECTION> as key.) In case of an error, C<$err> contains a textual error descriptions (C<undef> else).

A configuration string must comply to the following specifications:

=over 2

=item *

Configuration options may be grouped into sections. A section is declared by a line containing the section's name enclosed in square brackets, like C<[section_name]>.

=item *

Configuration options are given in the form C<option_name = option value>, i.e. option name and value are separated by an C<=> character, with the option's name not containing any whitespaces. Whitespaces at the line's beginning and end are ignored, as are spaces directly adjacent to the C<=>.

=item *

Comments are ignored. A Comment starts with a hash character C<#>; if a hash character is supposed to be part of an option name or value, it must be escaped like C<\#>.

=item *

Blank lines, or lines containing only whitespaces, are ignored.

=back

An example ini file might look like

  # The following two config options will be returned in
  # both the $defaultConf hash and the hashref stored in
  # $confSections->{$DEFAULT_INI_SECTION}
  foo = bar       # stored in $defaultConf->{foo}
  mumbo = jumbo   # guess where jumbo gets stored

  # Here a new section called 'system' is started. Its
  # options can be accesses via $confSections->{system}->{...}
  [system]
  path = /some/path
  file = my_file.txt

  # Yet another section...
  [graphics]
  resolution = 800x600
  description = This line contains an escaped \# character

=cut
sub parseConfigString {
  my @conf;
  if (scalar(@_) > 1) {
    @conf = @_
  }
  else {
    @conf = split(/\n/, $_[0]);
  }

  my @errs = ();
  my $conf = {};
  my $default = $conf;
  my $sections = {$DEFAULT_INI_SECTION => $conf};
  chomp(@conf);
  for (my $i = 0; $i < scalar(@conf); $i++) {
    my $line = $conf[$i];
    $line =~ s/^\s+//g;
    $line =~ s/\s+$//g;
    $line =~ s/^#.*$//g;
    $line =~ s/[^\\]#.*$//g;
    unless ($line eq "") {
      if ($line =~ m/(\S+)\s*=\s*(.*)/) {
        $conf->{$1} = $2;
      }
      elsif ($line =~ m/^\[(.+)\]$/) {
        my $section = $1;
        if (($section eq '') or ($section =~ m/^default$/i)) {
          $section = $DEFAULT_INI_SECTION;
        }
        if (exists($sections->{$section})) {
          $conf = $sections->{$section}
        }
        else {
          $conf = {};
          $sections->{$section} = $conf;
        }
      }
      else {
        push (@errs, "Syntax error in config at line " . ($i + 1) . ":\n>>>" . $conf[$i] . "<<<");
      }
    }
  }

  my $err = undef;
  if (scalar(@errs) > 0) {
    $err = join("\n", @errs) . "\n";
  }
  return ($default, $err, $sections);
}


=pod

=item * C<(hashref $defaultConf, string $err, hashref $allConfSections) readConfigFile([string $file])>

Reads the content of the text file specified by the given string and returns the result obtained by passing the file's content through the C<parseConfigString> function. (If no file with the given name/path can be found, the C<$file> string is considered to be the current application's file name and passed on into the search for config files at standard locations as described in the next paragraph.)

If no file is specified at all, this subroutine guesses some possible configuration file names and reads the first one it finds (if any). The file names searched for are constructed from the current application's file name (w/o extension, i.e. C<my_app.pl>'s C<< <application_name> >> would be C<my_app>): Initially, a file names C<< .<application_name> >> is searched for in the user's home directory. Next, the presence of files called C<< <application_name> >>, appended (in this order) with C<.conf>, C<.cnf>  and C<.ini>, is checked for.  Directories thus probed are (in this order) the present working directory, a putative C<< .<application_name> >> directory within the user's home directory, and the directory the application file itself is stored in.

=cut
sub readConfigFile {
  my $filename = undef;
  if (@_) {
    $filename = shift(@_);
  }
  unless ($filename && -e $filename) {
    my $path = &getDir($0);
    my $app = $filename ? $filename : &getName($0);
    my $home = "$HOMEDIR$DIRSEP.$app";
    if (-e "$HOMEDIR$DIRSEP.$app") {
      $filename = "$HOMEDIR$DIRSEP.$app";
    }
    elsif (-e "$CURRDIR$DIRSEP$app.conf") {
      $filename = "$CURRDIR$DIRSEP$app.conf";
    }
    elsif (-e "$CURRDIR$DIRSEP$app.cnf") {
      $filename = "$CURRDIR$DIRSEP$app.cnf";
    }
    elsif (-e "$CURRDIR$DIRSEP$app.ini") {
      $filename = "$CURRDIR$DIRSEP$app.ini";
    }
    elsif (-e "$home$DIRSEP$app.conf") {
      $filename = "$home$DIRSEP$app.conf";
    }
    elsif (-e "$home$DIRSEP$app.cnf") {
      $filename = "$home$DIRSEP$app.cnf";
    }
    elsif (-e "$home$DIRSEP$app.ini") {
      $filename = "$home$DIRSEP$app.ini";
    }
    elsif (-e "$path$DIRSEP$app.conf") {
      $filename = "$path$DIRSEP$app.conf";
    }
    elsif (-e "$path$DIRSEP$app.cnf") {
      $filename = "$path$DIRSEP$app.cnf";
    }
    elsif (-e "$path$DIRSEP$app.ini") {
      $filename = "$path$DIRSEP$app.ini";
    }
  }
  my $conf = {};
  my $err = undef;
  my $confs = undef;
  if (defined($filename)) {
    if (open(CONF, $filename)) {
      my @conf = <CONF>;
      CORE::close(CONF);
      ($conf, $err, $confs) = &parseConfigString(@conf);
    }
    else {
      $err = "Couldn't open config file $filename for reading!\n";
    }
  }
  return ($conf, $err, $confs);
}


=pod

=back

=cut

#--- Path Handling -----------------------------------


=pod

=head2 Path Handling

=over 1

=item * C<string cleanDir(string $dir)>

Returns the given path with any trailing directory separator removed; a string referring to the root directory, however, will not be changed.
Multiple consecutive instances of the current path separator will be replaced with a single instance. Furthermore, all path segments referring to the current directory (i.e. C<.>) will be removed, and so will pairs of segments involving the parent directory (C<..>).

=cut
sub cleanDir {
  my $dir = shift;
  $dir =~ s/$DIRSEP_REGEX+/$DIRSEP/g;
  unless ($dir eq $DIRSEP) {
    if (substr($dir, -1) eq $DIRSEP) {
      $dir = substr($dir, 0, length($dir)-1);
    }
  }
  while ($dir =~ s/[^$DIRSEP_REGEX]+$DIRSEP_REGEX\.\.$DIRSEP_REGEX//) {
  }
  $dir =~ s/$DIRSEP_REGEX\.$DIRSEP_REGEX/$DIRSEP/g;
  return $dir
}


=pod

=item * C<array splitPath(string $path)>

Splits the given path at all directory separators and returns the resulting parts as array

=cut
sub splitPath {
  my $path = shift;
  return split("/$DIRSEP_REGEX/", &cleanDir($path));
}


=pod

=item * C<string fullPath(string $path)>

Expands the given path in either of two ways (if applicable):

- If the path starts with the user's home directory, the leading 'C<~>' gets expanded to the value stored in the C<HOME> environment variable.

- If the path doesn't start with the system's directory separator, it is assumed to be relative and gets prepended with the current directory's absolute path.

=cut
sub fullPath {
  my $path = shift;
  $path =~ s/$DIRSEP_REGEX+/$DIRSEP/g;
  if ($path =~ m/^~($DIRSEP_REGEX.*)?$/) {
    $path = ($1 ? "$HOMEDIR$1" : $HOMEDIR);
  }
  elsif ($path =~ m/^\.($DIRSEP_REGEX.*)?$/) {
    $path = ($1 ? "$CURRDIR$1" : $CURRDIR);
  }
  elsif (not $path =~ m/^$DIRSEP_REGEX/) {
    $path = "$CURRDIR$DIRSEP$path";
  }
  return &cleanDir($path);
}


=pod

=item * C<string realPath(string $path, string root)>

If the given path (recursively) refers to a subdirectory of the given root, or a file residing within the root or (recursively) one of its subdirectories, its path relative to the root is returned. Otherwise, the file's/directorie's absolute path is returned. In case both paths are identical, an empty string is returned.

=cut
sub relPath {
  my $path = shift;
  my $root = shift;

  $path = &fullPath(&cleanDir($path));
  $root = &fullPath(&cleanDir($root));
  my $rootRegex = &regexEscape($root);
  if ($path eq $root) {
#     if (-d $root) {
#       $path = '.';
#     }
#     else {
#       (my $dir, my $file) = getPathSegments($root);
#       if ($dir) {
#         $path = $dir . $DIRSEP . $file;
#       }
#     }
    $path = '';
  }
  elsif ($path =~ m/^$rootRegex$DIRSEP_REGEX(.*)/) {
    $path = $1;
  }
  return $path;
}


=pod

=item * C<string getDir(string $path)>

Returns the directory segment of the given path (or an empty string, if no directory segment is present). Example:

  &getDir('/path/to/file.txt')

returns 'C</path/to>'.

=cut
sub getDir {
  my $path = shift;
  my $dir = "";
  if ($path =~ m/^(.*$DIRSEP_REGEX).+?$/) {
    $dir = $1;
  }
  return &cleanDir($dir);
}


=pod

=item * C<string getFile(string $path)>

Returns the file segment of the given path (or C<$path> itself, if no prepending directory segment is found). Example:

  &getFile('/path/to/file.txt')

returns 'C<file.txt>'.

=cut
sub getFile {
  my $path = shift;
  my $file = $path;
  if ($path =~ m/^.*$DIRSEP_REGEX(.+?)$/) {
    $file = $1;
  }
  return $file;
}


=pod

=item * C<string getName(string $path)>

Returns the a file's name, i. e. the part preceeding the file's extension (or the file's full designation, if no extension is found). Example:

  &getName('/path/to/file.txt')

returns 'C<file>'.

=cut
sub getName {
  my $path = shift;
  my $file = &getFile($path);
  my $name = $file;
  if ($file =~ m/^(.+)\..+?$/) {
    $name = $1;
  }
  return $name;
}


=pod

=item * C<string getExt(string $path)>

Returns the a file's extension, i. e. the part following the the last dot character "C<.>" within the file's designation (or an empty string, if no extension is found). Example:

  &getExt('/path/to/file.txt')

returns 'C<txt>'.

Leading dots (as in "dotfiles" like C<.bashrc>, which are "hidden" files on UNIX systems) are I<not> considered to initiate a file extension, i.e.

  &getExt('/path/to/.bashrc')

yields 'C<.bashrc>', whereas

  &getExt('/path/to/.bashrc.sh')

would still return an extension ('sh').

=cut
sub getExt {
  my $path = shift;
  my $file = &getFile($path);
  my $ext = "";
  if ($file =~ m/^.+\.(.+?)$/) {
    $ext = $1;
  }
  return $ext;
}


=pod

=item * C<(string $dir, string $file, string $name, string $ext) getPathSegments(string $path)>

Returns a list containing all results of all the above subroutine calls on the given path. Example:

  &getPathSegments('/path/to/file.txt')

returns ('C</path/to>', 'C<file.txt>', 'C<file>', 'C<txt>').

=cut
sub getPathSegments {
  my $path = shift;
  return (&getDir($path), &getFile($path), &getName($path), &getExt($path))
}


=pod

=item * C<string $err delDir(string $path, bool $contentOnly)>

Recursively deletes the directory specified by C<path> as well as all its content; if C<$contentOnly> is C<true>, the actual directory will be spared and only its content will be deleted. USE WITH EXTREME CARE!

=cut
sub delDir {

  my $err = undef;

  my $unlink = sub {
    if (!$err) {
      my $path = shift;
      unlink($path) || ($err = "Unable to remove '$path'!\n");
    }
  };

  my $rmdir = sub {
    if (!$err) {
      my $path = shift;
      rmdir($path) || ($err = "Unable to remove '$path'!\n");
    }
  };

  my $path = &fullPath(shift);
  my $contentOnly = shift;
  &visitFiles($path, $unlink, '*', '*', $rmdir, $VISIT_DIRS_BEFORE_FILES | $VISIT_DIRS_BEFORE_PROCESSING);
  unless ($contentOnly) {
    &{$rmdir}($path);
  }
  return $err;
}


=pod

=item * C<string makePath(string $path)>

Recursively creates all (sub-)directories in C<$path>. If case of an error, a textual error description will be returned (C<undef> else).

=cut
sub makePath {
  my $path = shift;
  $path = &fullPath($path);
  my $err = undef;
  (my $dir, my $file) = &getPathSegments($path);
  if (defined($dir) && ($dir ne '')) {
    $err = &makePath($dir);
  }
  unless ($err) {
    if (-e $path) {
      unless (-d $path) {
        $err = "File $path already exists!\n";
      }
    }
    else {
      unless (mkdir($path)) {
        $err = "Couldn't create directory $path!\n";
      }
    }
  }
  return $err;
}


=pod

=item * C<visitFiles(string $root, subref $fileAction [, string $filePattern [, string $dirPattern [, subref $dirAction [, int $order]]]])>

Starting from the given directory root, recursively processes all files and subdirectories complying to the specified file and directory glob patterns and calls the respective action routines, passing the currently visited file's or directory's name as first parameter. (The original C<$root> gets always passed as a second argument.)
If no file pattern is specified, 'C<*>' is used (i.e. all files are visited); the same is true for the directory pattern. No action gets performed on directories, if no directory action is explicitly specified.

Pass C<undef> as directory pattern to prevent recursion and to process onle those files residing in the given directory root.

Example:

  my $size = 0;
  &SysUtils::visitFiles('/usr/share/doc/perl', sub {my @s = stat($_[0]); $size += $s[7]}, '*.gz', '*', sub {print("Processing $_[0]\n")});
  print("Total size of gz files: $size\n");

will yield something like

  Processing /usr/share/doc/perl/CGI/eg
  Processing /usr/share/doc/perl/CGI
  Processing /usr/share/doc/perl/CPAN
  Processing /usr/share/doc/perl/ExtUtils
  Processing /usr/share/doc/perl/Net
  Total size of gz files: 2100221

Normally, directories are process (by applying the directory action) I<before> regular files (application of file action), and directory actions are applied I<before> the directory's content gets visited. The order of these steps can be changed by supplying a final argument, created by binary ORing (C<|>) a suitable selection of pre-defined constants:

C<$SysUtils::PROCESS_DIRS_BEFORE_FILES> vs. C<$SysUtils::PROCESS_FILES_BEFORE_DIRS>
(The use of C<$SysUtils::VISIT_DIRS_BEFORE_FILES> and C<$SysUtils::VISIT_FILES_BEFORE_DIRS> is deprecated.)

or

C<$SysUtils::PROCESS_DIRS_BEFORE_VISITING> vs. C<$SysUtils::VISIT_DIRS_BEFORE_PROCESSING>

=cut
sub visitFiles {

  sub doVisitDirs {
    my $root = shift;
    my $filePattern = shift;
    my $fileAction = shift;
    my $dirPattern = shift;
    my $dirAction = shift;
    my $flags = shift;
    my $ROOT = shift;

    my @paths = glob("$root$DIRSEP$dirPattern");
    if ($flags & $PROCESS_DOT_DIRS) {
      my @dots = glob("$root$DIRSEP.$dirPattern");
      push(@paths, grep(!/$DIRSEP_REGEX\.{1,2}$/, @dots));
    }
    @paths = sort(@paths);
    foreach my $path (@paths) {

      if (-d $path) {
        if (defined($dirAction) && !($flags & $VISIT_DIRS_BEFORE_PROCESSING)) {
          &{$dirAction}($path, $ROOT);
        }
        &doVisit($path, $filePattern, $fileAction, $dirPattern, $dirAction, $flags, $ROOT);
        if (defined($dirAction) && ($flags & $VISIT_DIRS_BEFORE_PROCESSING)) {
          &{$dirAction}($path, $ROOT);
        }
      }
    }
  }

  sub doVisitFiles {
    my $root = shift;
    my $filePattern = shift;
    my $fileAction = shift;
    my $dirPattern = shift;
    my $dirAction = shift;
    my $flags = shift;
    my $ROOT = shift;

    my @paths = glob("$root$DIRSEP$filePattern");
    if ($flags & $PROCESS_DOT_FILES) {
      push(@paths, glob("$root$DIRSEP.$filePattern"));
    }
    @paths = sort(@paths);
    foreach my $path (@paths) {
      unless (-d $path) {
        &{$fileAction}($path, $ROOT);
      }
    }
  }

  sub doVisit {
    my $root = shift;
    my $filePattern = shift;
    my $fileAction = shift;
    my $dirPattern = shift;
    my $dirAction = shift;
    my $flags = shift;
    my $ROOT = shift;

    if (defined($dirPattern) && !($flags & $VISIT_FILES_BEFORE_DIRS)) {
      &doVisitDirs($root, $filePattern, $fileAction, $dirPattern, $dirAction, $flags, $ROOT);
    }
    if (defined($fileAction)) {
      &doVisitFiles($root, $filePattern, $fileAction, $dirPattern, $dirAction, $flags, $ROOT);
    }
    if (defined($dirPattern) && ($flags & $VISIT_FILES_BEFORE_DIRS)) {
      &doVisitDirs($root, $filePattern, $fileAction, $dirPattern, $dirAction, $flags, $ROOT);
    }
  }

  my $root = &cleanDir(shift);
  my $fileAction = shift;
  my $filePattern = "*";
  if (@_) {
    $filePattern = shift;
  }
  my $dirPattern = "*";
  if (@_) {
    $dirPattern = shift;
  }
  my $dirAction = undef;
  if (@_) {
    $dirAction = shift;
  }
  my $flags = $VISIT_DIRS_BEFORE_FILES | $PROCESS_DIRS_BEFORE_VISITING;
  if (@_) {
    $flags = shift;
  }
  &doVisit($root, $filePattern, $fileAction, $dirPattern, $dirAction, $flags, $root);

}


=pod

=item * C<visitPaths(arrayref $paths, subref $fileAction [, string $filePattern [, string $dirPattern [, subref $dirAction [, int $order]]]])>

For each path in C<$paths>, either call the referenced file action routine (if the current path refers to a regular file), or calls C<&visitDirs>, using the current path as root (if the current path refers to a directory).

=cut

sub visitPaths {
  my $paths = shift;
  my $fileAction = shift;
  foreach my $path (@{$paths}) {
    $path = &SysUtils::fullPath($path);
    if (-d $path) {
      &SysUtils::visitFiles($path, $fileAction, @_);
    }
    else {
      my $root = &getDir($path);
      &{$fileAction}($path, $root);
    }
  }
}


=pod

=back

=cut

#--- I/O-Related Stuff ------------------------------------


#--- Private I/O functions

sub __doLock {
  my $lockType = shift;
  my $handle= shift;
  my $nonBlocking = 0;
  my $timeout = 0;
  if (@_) {
    $nonBlocking = 1;
    $timeout = shift;
  }
  my $t0 = time;
  my $locked = 0;
  do {
    $locked = flock ($handle, $lockType | ($nonBlocking ? LOCK_NB : 0));
    unless ($locked) {
      sleep(1);
    }
  } while ($nonBlocking and !$locked and ((time - $t0) < $timeout));
  return $locked;
}


sub __doOpen {
  my $path = shift;
  my $openMode = shift;
  my $lockType = shift;

  my $handle = undef;
  if (sysopen ($handle, $path, $openMode)) {
    unless (&__doLock($lockType, $handle, @_)) {
      $handle = undef;
    }
  }
  return $handle
}


#--- Public I/O functions

=pod

=head2 I/O-Related Stuff

=over 1

=item * C<string copyFile(string $source, string $target [, int $bufferSize])>

Copies the content of the file specified by its C<$source> path into a (new) file at C<$target>. This is done by chunk-wise reading the source file into a 4 kB buffer and writing it back to the target file. The buffer size can be adjusted via the optional C<$bufferSize> argument.

In case of an error, an error message is returned (C<undef> else).

I<Please note that this function will overwrite any target file which happens to exist already!>

=cut
sub copyFile {
  my $source = shift;
  my $target = shift;
  my $bufferSize = 65536;
  if (@_) {
    $bufferSize = shift;
  }
  my $err = undef;
  my $buffer = '';
  if (open(SOURCE, $source)) {
    if (open(TARGET, ">$target")) {
      seek(SOURCE, 0, 0);
      until (eof(SOURCE)) {
        read(SOURCE, $buffer, $bufferSize);
        print TARGET ($buffer);
      }
      CORE::close(TARGET);
    }
    else {
      $err = "Couldn't open \"$target\" for writing!\n";
    }
    CORE::close(SOURCE);
  }
  else {
    $err = "Couldn't open \"$source\" for reading!\n";
  }
  return $err;
}


=pod

=item * C<string moveFile(string $source, string $target [, int $bufferSize])>

Moves (and renames) the file specified by its C<$source> path to the location given in C<$target>. This is done first copying the source file using the C<copyFile> function and subsequently deleting the source file (if copying succeeded).

In case of an error, an error message is returned (C<undef> else).

I<Please note that this function will overwrite any file which happens to already exist at the target location!>

=cut
sub moveFile {
  my $source = shift;
  my $target = shift;
  my $err = &copyFile($source, $target, @_);
  unless (defined($err)) {
    unlink($source);
  }
  return $err;
}


=pod

=item * C<string writeToFile(string $path, list @items)>

Tries to write the elements of C<@items> to the file specified by C<$path>. If any item turns out to be a reference to a list or a scalar, it will be dereferenced prior to output.

In case of an error, an error message is returned (C<undef> else).

I<Please note that this function will overwrite any file which happens to already exist at the target location!>

=cut
sub writeToFile {
  my $path = shift;
  my $err = undef;
  if (-d $path) {
    $err = "Cannot write to directory \"$path\"!\n";
  }
  else {
    if (open(FILE, ">$path")) {
      foreach my $item (@_) {
        if (ref($item) eq 'ARRAY') {
          print FILE (@{$item});
        }
        elsif (ref($item) eq 'SCALAR') {
          print FILE (${$item});
        }
        else {
          print FILE ($item);
        }
      }
      CORE::close(FILE);
    }
    else {
      $err = "Couldn't open \"$path\" for writing!\n";
    }
  }
  return $err;
}


=pod

=item * C<(strref $data, string $err) readFromFile(string $path)>

Returns a reference to a string containing the content of the whole file specified by C<$path>.

In case of an error, an error message is returned in C<$err> (C<undef> else).

=cut
sub readFromFile {
  my $path = shift;
  my $err = undef;

  my $data = '';
  if (-d $path) {
    $err = "Cannot read from directory \"$path\"!\n";
  }
  else {
    if (open(FILE, $path)) {
      my @data = <FILE>;
      CORE::close(FILE);
      $data = join('', @data);
    }
    else {
      $err = "Couldn't open \"$path\" for reading!\n";
    }
  }
  return (\$data, $err);
}


=pod

=item * C<boolean shlock(handle $handle [, int $timeout])>

Tries to obtain a shared lock on the file referenced by C<$handle>. If a timeout value (in seconds) is given, locking attempts are repeatedly issued until a lock is gained or the specified time has passed.

Returns C<TRUE> or C<FALSE> depending on whether or not locking could be achieved.

=cut
sub shlock {
  return &__doLock(LOCK_SH, @_);
}


=pod

=item * C<boolean exlock(handle $handle [, int $timeout])>

Tries to obtain an exclusive lock on the file referenced by C<$handle>. If a timeout value (in seconds) is given, locking attempts are repeatedly issued until a lock is gained or the specified time has passed.

Returns C<TRUE> or C<FALSE> depending on whether or not locking could be achieved.

=cut
sub exlock {
  return &__doLock(LOCK_EX, @_);
}


=pod

=item * C<boolean unlock(handle $handle)>

Removes a previously obtained lock from the file specified by C<$handle>.

=cut
sub unlock {
  my $handle = shift;
  my $unlocked = flock($handle, LOCK_UN);
  return $unlocked;
}


=pod

=item * C<handle ropen(string $path [, int $timeout])>

Tries to open the file specified by C<$path> for reading only and simultaneously applies a shared lock (using C<shlock>), preventing the file from being modified by other processes. Returns a handle for the opened file if opening and locking was successful (C<undef> else).

=cut
sub ropen {
  my $path = shift;
  return &__doOpen($path, O_RDONLY, LOCK_SH);
}


# =pod
#
# =item * C<handle wopen(string $path [, int $timeout])>
#
# Tries to open the file specified by C<$path> for writing only and simultaneously applies an exclusive lock (using C<exlock>), preventing the file from being modified or read by other processes. Returns a handle to the opened file if opening and locking was successful (C<undef> else).
#
# =cut
# sub wopen {
#   my $path = shift;
#   return &__doOpen($path, O_WRONLY | O_CREAT, LOCK_EX);
# }


=pod

=item * C<handle rwopen(string $path [, int $timeout])>

Tries to open the file specified by C<$path> for reading and writing and simultaneously applies an exclusive lock (using C<exlock>), preventing the file from being modified or read by other processes. Returns a handle to the opened file if opening and locking was successful (C<undef> else).

=cut
sub rwopen {
  my $path = shift;
  return &__doOpen($path, O_RDWR | O_CREAT, LOCK_SH);
}


=pod

=item * C<close(handle $handle)>

Removes any lock on the file specified by C<$handle> prior to closing it.

=cut
sub close {
  my $handle = shift;
  &unlock ($handle);
  CORE::close($handle);
}


=pod

=item * C<(stringRef $out, int $exitCode) execCommand(list @cmd)>

Executes the command specified in @_, just as C<system> does. Returns the command's standard output as a refernce to a single string, as well as the command's exit code (or an error message, if the command couldn't be launched).

=cut
sub execCommand {
  return &_execAndCapture(0, @_);
}

=pod

=item * C<(int $pid, string $err, handle $stdout) execInBackground(list @cmd)>

Executes the command specified in @_ and returns immediately. Returns the background command's pid, an error message (C<undef> if everything has gone well), and a handle to the background task's STDOUT. Note: You B<must> readout STDOUT; othwerwise, your program may hang! (If you don't want to handle STDOUT yourself, use C<execSilentlyInBackground> instead.)

=cut
sub execInBackground {
  return &_exec(1, @_);
}

=pod

=item * C<(int $pid, string $err, handle $stdout) execInBackground(list @cmd)>

Executes the command specified in @_ and returns immediately. Returns the background command's pid and an error message (C<undef> if everything has gone well).

=cut
sub execSilentlyInBackground {
  my $err = undef;
  my $pid = fork();
  if (defined($pid)) {
    unless ($pid) {
      setpgrp(0, 0);
      &_execAndCapture(0, @_);
      exit;
    }
  }
  else {
    $err = "Couldn't initiate fork for execution of @_\n";
  }
  return ($pid, $err);
}

=pod

=back

=cut

sub _exec {
  my $regroup = shift;
  my $err = undef;
  my $pid = open(my $stdout, '-|');
  if (defined($pid)) {
    unless ($pid) {
      # Child
      if ($regroup) {
        setpgrp(0, 0);
      }
      system(@_);
      exit($? >> 8);
    }
  }
  else {
    $err = "Couldn't initiate fork for execution of @_\n";
  }
  return ($pid, $err, $stdout);
}

sub _execAndCapture {
  my $regroup = shift;
  (my $pid, my $err, my $stdout) = &_exec($regroup, @_);
  my $out = '';
  unless ($err) {
    while (!eof($stdout)) {
      $out .= <$stdout>;
    }
    CORE::close($stdout);
  }
  return (\$out, $err);
}


#--- UI Stuff ---------------------------------------------


=pod

=head2 UI Stuff

=over 1

=item * C<char getKeystroke()>

A quick-and-dirty hack for entering a single keystroke from the console, without requiring the user to hit the [ENTER] key.

Currently only works on UNIX systems running the bash shell.

=cut
sub getKeystroke {
  my $cmd = 'read -r -s -n1 -d \'\' __KEY__; echo -n $__KEY__';
  return qx($cmd);
}


=pod

=back

=cut

#--- Data-Structure related stuff -------------------------


=pod

=head2 Data-Structure Related Stuff

=over 1

=item * C<int indexOf(scalar $what, array @list)>

Returns the index of the first occurance of the given element in the remaining argument list, or -1 if no such element is found. Example:

  &indexOf('c', 'a', 'b', 'c', 'b', 'c')

will return 2.

=cut
sub indexOf {
  my $elem = shift;
  my $pos = -1;
  for (my $i = 0; $i < scalar(@_); $i++) {
    if ($elem eq $_[$i]) {
      $pos = $i;
      last;
    }
  }
  return $pos;
}


=pod

=item * C<mergeOverwrite(hashref $one, hashref $other)>

Copies each entry of C<$other> into C<$one>, regardless of whether an identically named key already exists, thereby possibly overwriting values stored in C<$one>.

=cut
sub mergeOverwrite {
  my $one = shift;
  my $other = shift;
  foreach my $key (keys(%{$other})) {
    $one->{$key} = $other->{$key};
  }
}


=pod

=item * C<mergeAdd(hashref $one, hashref $other)>

Copies only those entries of C<$other> into C<$one> for which no key does already exist.

=cut
sub mergeAdd {
  my $one = shift;
  my $other = shift;
  foreach my $key (keys(%{$other})) {
    unless (exists($one->{$key})) {
      $one->{$key} = $other->{$key};
    }
  }
}


=pod

=item * C<mergeUpdate(hashref $one, hashref $other)>

Copies only those entries of C<$other> into C<$one> for which a key does already exist, thereby overwriting the original value in C<$one>.

=cut
sub mergeUpdate {
  my $one = shift;
  my $other = shift;
  foreach my $key (keys(%{$other})) {
    if (exists($one->{$key})) {
      $one->{$key} = $other->{$key};
    }
  }
}



=pod

=item * C<string dumpRef(ref $ref [, int $indent [, int $maxDepth]])>

Returns a human-readable string recursively describing the data structure given in C<$ref>, including each reference's type and memory address. To prevent infinite recursion, references already seen before are not dumped again; instead, only their memory address is inserted into the dump, allowing for  re-identification of previously seen references.

Normally, hash and array elements are all dumped in one line of text; if, however, a positive integer value is passed in C<$indent>, elements get recursively indented by the specified amount (x 4 space characters), one element per line.

Negative indentation values have, in principle, the same effect as their positive counterparts, except that only hash elements are indented, while arrays are dumped in the usual, compact one-lined form.

If a C<$maxDepth> is given, recursion ends at the specified maximum depth.

=cut
sub dumpRef {

  sub doDump {
    my $ref = shift;
    my $depth = shift;
    my $str = shift;
    my $seen = shift;
    my $maxDepth = shift;
    my $indent = shift;

    my $indStr = ' ' x ($depth * abs($indent));

    if (defined($ref)) {

      if (ref($ref)) {

        if (exists($seen->{$ref})) {
          ${$str} .= "<$ref>";
        }
        else {
          $seen->{$ref} = $ref;
          if ($ref =~ m/^(.+=)?(ARRAY\(.+\))/) {
#           if (ref($ref) =~ m/^ARRAY$/) {
            my $arrayRef = $ref;
            if (($maxDepth < 1) || ($depth < $maxDepth)) {
              if (($depth > 0) && ($indent > 0)) {
                ${$str} .= "\n$indStr$ref: ";
              }
              else {
                ${$str} .= "$ref: ";
              }
              ${$str} .= '[';
              my $n = scalar(@{$arrayRef}) - 1;
              for(my $i = 0; $i <= $n; $i++) {
                if ($indent > 0) {
                  ${$str} .= "\n$indStr  $i : ";
                }
                &doDump($ref->[$i], $depth+1, $str, $seen, $maxDepth, $indent);
                unless ($i == $n) {
                  ${$str} .= ', ';
                }
              }
              if ($indent > 0) {
                ${$str} .= "\n$indStr";
              }
              ${$str} .= ']';
            }
            else {
              ${$str} .= "$ref: [... " . scalar(@{$arrayRef}) . ' ...]';
            }
          }
          elsif ($ref =~ m/^(.+=)?(HASH\(.+\))/) {
            my $hashRef = $ref;
            if (($maxDepth < 1) || ($depth < $maxDepth)) {
              if (($depth > 0) && ($indent != 0)) {
                ${$str} .= "\n$indStr$ref: ";
              }
              else {
                ${$str} .= "$ref: ";
              }
              ${$str} .= '{';
              my @keys = sort(keys(%{$hashRef}));
              my $n = scalar(@keys) - 1;
              for(my $i = 0; $i <= $n; $i++) {
                if ($indent != 0) {
                  ${$str} .= "\n$indStr  ";
                }
                ${$str} .= "$keys[$i] => ";
                &doDump($ref->{$keys[$i]}, $depth+1, $str, $seen, $maxDepth, $indent);
                unless ($i == $n) {
                  ${$str} .= ', ';
                }
              }
              if ($indent != 0) {
                ${$str} .= "\n$indStr";
              }
              ${$str} .= '}';
            }
            else {
              ${$str} .= "$ref: [... " . scalar(keys(%{$hashRef})) . ' ...]';
            }
          }
          elsif (ref($ref) eq 'SCALAR') {
            ${$str} .= "$ref: " . ${$ref};
          }
          else {
            ${$str} .= $ref;
          }
        }
      }
      else {
        ${$str} .= $ref;
      }
    }
    else {
      ${$str} .= '<undef>';
    }
  }

  my $ref = shift;
  my $indent = 0;
  if (@_) {
    $indent = shift;
  }
  my $maxDepth = 0;
  if (@_) {
    $maxDepth = shift;
  }
  my $str = '';
  &doDump($ref, 0, \$str, {}, $maxDepth, 4*$indent);
  return $str;
}


=pod

=item * C<string printRefDump(ref $ref [, int $indent [, int $maxDepth]])>

Immediately prints a human-readable dump of the given data structure to C<STDOUT>.

=cut
sub printRefDump {
  print(&dumpRef(@_), "\n");
}


=pod

=back

=cut


#--- String and regex related stuff -----------------------


=pod

=head2 String and Regex Related Stuff

=over 1

=item * C<string trim(string $str)>

Removes any leading and/or trailing whitespace from C<$str>.

=cut

sub trim {
  my $str = shift;
  $str =~ s/(^\s+|\s+$)//g;
  return $str;
}


=item * C<string hex(int $n [, int $digits])>

Returns the hexadecimal representation of the given (positiv) integer C<$n>. If a second argument is passed, the return string will be padded with zeros in order to reach a length of C<$digits> characters.

=cut
sub hex {
  my $n = shift;
  my $d = undef;
  if (@_) {
    $d = shift;
  }
  if (defined($d)) {
    return sprintf("%0${d}x", $n);
  }
  else {
    return sprintf("%x", $n);
  }
}


=pod

=item * C<string shellEscape(string $str)>

Escapes in the given C<$str> all shell metacharactes.

=cut
sub shellEscape {
  my $str = shift;
  $str =~ s/([;<>\*\|`&\$!#\(\)\[\]\{\}:'"])/\\$1/g;
  return $str;
}

=pod

=item * C<string pathEscape(string $str)>

Performs a C<shellEscape> on the given string and additionally escapes space characters.

=cut
sub pathEscape {
  my $str = &shellEscape(shift);
  $str =~ s/ /\\ /g;
  return $str;
}


=pod

=item * C<string regexEscape(string $str)>

Converts the given C<$str> so that it can be used as a search pattern in a regular expression, i.e. all regex metacharacters (such as 'C<(>', 'C<)>', 'C<+>' or 'C<*>') get escaped by prepending a backslash 'C<\>'.

=cut
sub regexEscape {
  my $str = shift;
  $str =~ s/([\\\/\[\](){}\^\$.|*+?])/\\$1/g;
  $str =~ s/\n/\\n/g;
  $str =~ s/\t/\\t/g;
  $str =~ s/\r/\\r/g;
  return $str;
}


=pod

=item * C<string fileGlobEscape(string $str)>

Assuming that C<$str> represents a (double-quoted) filename, possibly containing any of the wildcards C<*> and C<?>, returns a regular expression suitable for checking whether some other filename would be covered by C<$str>. Note: locators C<^> and C<$> (for 'match at string start' and 'match at string end', respectively) are NOT included in the retured regex.

=cut
sub fileGlobEscape {
  my $str = shift;
  $str =~ s/([\\\/\[\](){}\^\$.|+])/\\$1/g;
  $str =~ s/\n/\\n/g;
  $str =~ s/\t/\\t/g;
  $str =~ s/\r/\\r/g;
  $str =~ s/(^\*)/.*/g;
  $str =~ s/([^\\])\*/$1.*/g;
  return $str;
}

=pod

=item * C<string htmlEscape(string $str)>

Returns a version of C<$str> in which all characters outside the ASCII range of 0x20 - 0x7f are represented as HTML entitites of the form 'C<&nnn;>'. Additionally, the HTML metacharacters 'C<&>', 'C<< < >>' and 'C<< > >>' also get escaped.

=cut
sub htmlEscape {
  my $str = shift;
  $str =~ s/([^\x20-\x25\x27-\x3b\x3d\x3f-\x7f])/'&#' . ord($1) . ';'/ge;
#  $str =~ s/([\x26\x3c\x3e])/'&#' . ord($1) . ';'/ge;
  return $str;
}


=pod

=item * C<string urlEscape(string $str)>

Escapes all characters except letters, digits, underscore and dash in an url-encoding manner, i.e. by substituting them with 'C<%>', followed by their hex value.

=cut
sub urlEscape {
  my $str = shift;
  $str =~ s/([^\w_-])/'%' . &hex(ord($1), 2)/ge;
  return $str
}


=pod

=item * C<string qqEscape(string $str [, string $chars])>

Escapes all (single and double) quotes and backslashes, as well as all characters additionally passed. Furthermore, replaces line breaks with (literally!) 'C<\n>', carriage returns with 'C<\r>' and tabs with 'C<\t>'. The resulting string could e.g. be literally used in double-quoted manner within some Perl source code.

=cut
sub qqEscape {
  my $str = shift;
  $str =~ s/([\\"'])/\\$1/g;
  while (@_) {
    my $chars = &regexEscape(shift);
    $str =~ s/([$chars])/\\$1/g;
  }
  $str =~ s/\n/\\n/g;
  $str =~ s/\r/\\r/g;
  $str =~ s/\t/\\t/g;
  return $str;
}


=pod

=item * C<string qqUnescape(string $str [, string $chars])>

Reverse function of C<qqEscape>.

=cut
sub qqUnescape {
  my $str = shift;
#print "qqUnescape: $str => ";
  $str =~ s/(\A\\n|[^\\]\\n)/\n/g;
  $str =~ s/(\A\\r|[^\\]\\r)/\r/g;
  $str =~ s/(\A\\t|[^\\]\\t)/\t/g;
  while (@_) {
    my $chars = shift;
    $str =~ s/(\A\\([$chars])|[^\\]\\([$chars]))/$2/g;
  }
  $str =~ s/(\A\\(["'])|[^\\]\\(["']))/$2/g;
  $str =~ s/\\\\/\\/g;
#print "$str\n";
  return $str;
}

=pod

=item * C<hash extractMatches(stringref $str, string $regex [, string $delim])>

This function replaces in the string referenced by C<$str> all (non-overlapping) occurences of the pattern described by C<$regex> with markers of the form C<$delim>I<n>C<$delim>, with I<n> being counted up starting from zero. If no delimiter is given, 'C<###>' is used.

All pairs of markers and corresponding pattern occurances are returned as a hash.

=cut
sub extractMatches {

  sub findNextMarker {
    my $str = shift;
    my $delim = shift;
    my $marker = "$delim$MARKER_COUNTER$delim";
    pos(${$str}) = 0;
    while (${$str} =~ m/$marker/g) {
      $MARKER_COUNTER++;
      $marker = "$delim$MARKER_COUNTER$delim";
    }
    return $marker;
  }

  my $str = shift;
  my $pattern = shift;
  my $delim = '###';
  if (@_) {
    $delim = shift;
  }

  my %matches = ();

  my $marker = &findNextMarker($str, $delim);
  my $done = 0;
  until ($done) {
    if (${$str} =~ s/($pattern)/$marker/msi) {
      $matches{$marker} = $1;
      $marker = &findNextMarker($str, $delim);
    }
    else {
      $done = 1;
    }
  }

  return %matches;
}


=pod

=item * C<reinsertAtMarker(stringref $str, string $replace, string $marker)>

A convenience method which just inserts the given C<$replace> string at the location marked by C<$marker> into the string referenced by $str.

=cut
sub reinsertAtMarker {
  my $str = shift;
  my $replace = shift;
  my $marker = shift;

  ${$str} =~ s/$marker/$replace/msi;
}

=pod

=back

=cut

#--- End of SysUtils --------------------------------------

1;
