CHANGELOG SysUtils.pm
=====================

1.5.1
-----

- Fixed bug in execSilentlyInBackground which caused a
  child process not to properly terminate.


1.5.0
-----

- Renamed functions:
  - mergeAddAll => mergeOverwrite
  - mergeAddMissing => mergeAdd
  - mergeChangeExisting => mergeUpdate


1.4.2
-----

- Complete rewrite of execCommand and execInBackground; the latter now
  returns pid, error message, and a handle to the background command's
  STDOUT. Additionally: execSilentlyInBackground
- new delDir function to recursively delete a directory (optionally:
  only its contents)
- readConfigFile: Standard location are only scanned, if the (optionally)
  passed path is not found.


1.4.1
-----

- New execInBackground function


1.4.0
-----

- strip(str) renamed to trim(str)


1.3.9
-----

- visitFiles now can handle hidden files (dot files)
- new visitPaths function for visiting multiple paths to both dirs and files
- both visitFiles and visitPaths provide the original root directory as
  second argument to fileAction and dirAction
- improved cleanDir function
- shellEscape for escaping shell meta characters
- pathEscape for additionally escaping space characters


1.3.8
-----

- Export by default the imported File::Glob/File::DosGlob glob function
- New "strip" function for removing leading/trailing whitespace from a string.
- Improved cleanDir function (now also removes "./" and "something/../")
- Improved dumpRef to correctly handle blessed references


1.3.7
-----

- New "relPath" function for obtaining a path's subpath relative to some root directory.
- New "fileGlobEscape" for obtaining a regex pendant to a path containing wildcards.


1.3.6
-----
- Fixed bug in &readConfigFile (use of literal 'app' instead of variable $app when looking for config file ~/.<application_name>).